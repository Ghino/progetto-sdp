package services;

import beans.GlobalLocalStats;
import beans.Node;
import beans.NodesList;
import cloud.Nodes;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("node")
public class NodeService {

    @Path("add/node")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addNode(Node n){
        NodesList result = Nodes.getInstance().addNode(n);
        if(result.getNodeList().get(0).getId() == -1){
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        if(result.getNodeList().get(0).getId() == -2){
            return Response.status(Response.Status.PRECONDITION_FAILED).build();
        }
        if(result.getNodeList().get(0).getId() == -3){
            return Response.status(Response.Status.CONFLICT).build();
        }
        return Response.ok(result).build();
    }

    @Path("close/nodes/{node}")
    @DELETE
    public Response removeNode(@PathParam("node") int n){
        if(!Nodes.getInstance().removeNode(n)){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().build();
    }

    @Path("update/stats")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateGlobal(GlobalLocalStats values){ //aggiornamento valori globali e dei singoli nodi

        Nodes.getInstance().updateStats(values);
        return Response.ok().build();
    }

}
