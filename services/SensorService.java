package services;

import beans.Node;
import cloud.Nodes;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("sensor")
public class SensorService {

    @Path("find")
    @GET
    @Produces({"application/json", "application/xml"})
    public Response findClosestNode(@QueryParam("x") int x, @QueryParam("y") int y){

        Node result = Nodes.getInstance().closestNode(x, y);

        if(result == null){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.ok(result).build();
    }
}
