package services;


import beans.*;
import cloud.Nodes;
import edges.Stat;
import javafx.util.Pair;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("analist")
public class AnalistService {

    //restituisce le posizioni dei nodi edge
    @Path("get/locations")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNodesPosition(){

        List<Node> nodi = Nodes.getInstance().getNodesListCopy();
        if (nodi.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        try{
            //Thread.sleep(7000);
        } catch(Exception e){

        }

        NodesPosition result = new NodesPosition();

        for(Node n : nodi){
            NodePosition position = new NodePosition();
            position.setId(n.getId());
            position.setX(n.getPosx());
            position.setY(n.getPosy());
            result.getPositions().add(position);
        }

        return Response.ok(result).build();

    }

    //permette di ottenere le ultime n statistiche prodotte da un nodo edge
    @Path("get/statistics/nodes/{node}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNStatisticsFromNode(@PathParam("node")int nodo, @QueryParam("n") int n){

        List<Node> nodes = Nodes.getInstance().getNodesListCopy();
        Measurements result = new Measurements();
        for(Node i : nodes) {
            if (i.getId() == nodo){
                List<Stat> check = i.getNMeasurements(n);
                if(check == null){
                    return Response.status(Response.Status.BAD_REQUEST).build();
                }
                result.getValuesList().addAll(check);
            }
        }
        if(result.getValuesList().isEmpty()){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(result).build();

    }

    //permette di ottenere le ultime n statistiche prodotte globalmente e localmente nella città (considerare il punto che il primo nodo è il coordinatore)
    @Path("get/statistics")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNCityStatistics(@QueryParam("n")int n){

        Pair<List<Stat>, List<Stat>> stats = Nodes.getInstance().getStatsCopy();
        List<Stat> localStats = stats.getValue();
        List<Stat> globalStats = stats.getKey();
        Measurements result = new Measurements();
        if(globalStats.size() >= n && localStats.size() >= n){
            for(int i = globalStats.size() - n; i < globalStats.size(); i++) {
                result.getValuesList().add(globalStats.get(i));
            }
            for(int i = localStats.size() - n; i < localStats.size(); i++) {
                result.getValuesList().add(localStats.get(i));
            }
            /*for(Node i : nodes) {
                if(i.getNMeasurements(n) != null){
                    result.getValuesList().addAll(i.getNMeasurements(n));
                }
            }*/
        }
        if(result.getValuesList().isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.ok(result).build();
    }

    //permette di ottenere la deviazione standard e la media delle ultime n statistiche di un nodo edge
    @Path("get/statistics/average_variance/nodes/{node}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getAverageVarianceFromNode(@PathParam("node")int nodo, @QueryParam("n")int n){

        List<Node> nodes = Nodes.getInstance().getNodesListCopy();
        double average = 0;
        double variance = 0;
        List<Double> values = new ArrayList<>();
        for(Node a : nodes){
            if(a.getId() == nodo){
                if(a.getMeasurements().size() < n){
                    return Response.status(Response.Status.NO_CONTENT).build();
                }
                List<Stat> measures = a.getNMeasurements(n);
                for(int i = 0; i < n; i++){
                    average += measures.get(i).getValue();
                    values.add(measures.get(i).getValue());
                }
                break;
            }
        }

        if(average == 0){
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        average = average / n;

        double temp = 0;
        for(double a : values)
            temp += (a - average)*(a - average);
        variance = temp/(n - 1);

        return Response.ok("Average: " + average + "\nVariance: " + variance).build();
    }

    //permette di ottenere la deviazione standard e la media delle ultime n statistiche della città
    @Path("get/statistics/average_variance")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getAverageVarianceFromCity(@QueryParam("n")int n){

        List<Stat> globalStats = Nodes.getInstance().getGlobalStatsCopy();
        if(globalStats.size() < n){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        double average = 0;
        double variance = 0;
        List<Double> values = new ArrayList<>();
        int size = globalStats.size();
        for(int i = size - n; i < size; i++){
            average += globalStats.get(i).getValue();
            values.add(globalStats.get(i).getValue());
        }

        average = average / n;

        double temp = 0;
        for(double a : values)
            temp += (a - average)*(a - average);
        variance = temp/(n - 1);

        return Response.ok("Average: " + average + "\nVariance: " + variance).build();
    }
}
