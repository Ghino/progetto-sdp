import beans.Measurements;
import beans.NodesPosition;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AnalistClient {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static Client client;

    public static void main(String[] args) {

        client = Client.create();

        try{
            WebResource target = client.resource("http://localhost:1337/analist").path("get/locations");
            ClientResponse response = target.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        } catch (Exception e){
            System.out.println("Il server non è attivo.");
            return;
        }

        System.out.println("Client avviato!");

        boolean exit = false;

        while(!exit){

            System.out.println("\nInserisci uno dei seguenti numeri per la relativa funzione:");
            System.out.println("1. Posizioni dei nodi nella città");
            System.out.println("2. Ultime n statistiche di un determinato nodo");
            System.out.println("3. Ultime n statistiche globali e locali della città");
            System.out.println("4. Media e varianza delle ultime n statistiche di un determinato nodo");
            System.out.println("5. Media e varianza delle ultime n statistiche della città");
            System.out.println("6. Chiudere il client\n");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            try{
                int value = Integer.valueOf(br.readLine());
                switch (value){
                    case 1:
                        nodePos();
                        break;
                    case 2:
                        nStatNode();
                        break;
                    case 3:
                        nStatCity();
                        break;
                    case 4:
                        avgDevNode();
                        break;
                    case 5:
                        avgDevCity();
                        break;
                    case 6:
                        client.destroy();
                        exit = true;
                }
            } catch (Exception e){
                e.printStackTrace();
                System.out.println("Inserito carattere non valido, riprova.");
            }
        }

    }

    private static void nodePos(){
        WebResource target = client.resource("http://localhost:1337/analist").path("get/locations");
        ClientResponse response = target.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if(response.getStatus() == 204){
            System.out.println("Non ci sono nodi");
            return;
        }
        NodesPosition result = response.getEntity(NodesPosition.class);
        response.close();
        System.out.println("RESPONSE:");
        for (int i = 0; i < result.getPositions().size(); i++) {
            System.out.println("NODE: " + result.getPositions().get(i).getId() + " POSITION: " + result.getPositions().get(i).getX() + "," + result.getPositions().get(i).getY());
        }
    }

    private static void nStatNode(){

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int nodo = 0;
        int n = 0;

        while(true){
            try {
                System.out.println("Inserire id del nodo da analizzare:");
                nodo = Integer.valueOf(br.readLine());
                System.out.println("Inserire il numero di misurazioni da leggere:");
                n = Integer.valueOf(br.readLine());
                break;
            } catch (Exception e){
                System.out.println("Inserito carattere non valido, riprova.");
            }
        }

        WebResource target = client.resource("http://localhost:1337/analist").path("get/statistics/nodes/" + nodo).queryParam("n", String.valueOf(n));
        ClientResponse response = target.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if(response.getStatus() == 400){
            System.out.println("Il nodo non ha ancora così tante misurazioni.");
            return;
        }
        if(response.getStatus() == 404){
            System.out.println("Il nodo non esiste.");
            return;
        }
        Measurements values = response.getEntity(Measurements.class);
        response.close();
        System.out.println("RESPONSE FROM NODE " + nodo + ":");
        for (int i = 0; i < values.getValuesList().size(); i++) {
            System.out.println("VALUE: " + values.getValuesList().get(i).getValue() + " DATE: " + sdf.format(new Date(values.getValuesList().get(i).getTimestamp())));
        }
    }

    private static void nStatCity(){

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int n = 0;

        while(true){
            try {
                System.out.println("Inserire il numero di misurazioni da leggere:");
                n = Integer.valueOf(br.readLine());
                break;
            } catch (Exception e){
                System.out.println("Inserito carattere non valido, riprova.");
            }
        }

        WebResource target = client.resource("http://localhost:1337/analist").path("get/statistics").queryParam("n", String.valueOf(n));
        ClientResponse response = target.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if(response.getStatus() == 204){
            System.out.println("Non sono ancora state prodotte così tante misure.");
            return;
        }
        Measurements values = response.getEntity(Measurements.class);
        response.close();
        for (int i = 0; i < values.getValuesList().size(); i++) {
            if(i == 0){
                System.out.println("GLOBALI:");
            }
            if(i == n){
                System.out.println("LOCALI:");
            }
            System.out.println("VALUE: " + values.getValuesList().get(i).getValue() + " DATE: " + sdf.format(new Date(values.getValuesList().get(i).getTimestamp())));
        }
    }

    private static void avgDevNode(){

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int nodo = 0;
        int n = 0;

        while(true){
            try {
                System.out.println("Inserire id del nodo da analizzare:");
                nodo = Integer.valueOf(br.readLine());
                System.out.println("Inserire il numero di misurazioni da leggere:");
                n = Integer.valueOf(br.readLine());
                break;
            } catch (Exception e){
                System.out.println("Inserito carattere non valido, riprova.");
            }
        }

        WebResource target = client.resource("http://localhost:1337/analist").path("get/statistics/average_variance/nodes/" + nodo).queryParam("n", String.valueOf(n));
        ClientResponse response = target.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        if(response.getStatus() == 204){
            System.out.println("Non sono ancora state prodotte così tante misure.");
            return;
        }
        if(response.getStatus() == 404){
            System.out.println("Il nodo non esiste.");
            return;
        }
        System.out.println("RESPONSE FROM NODE " + nodo + ":");
        System.out.println(response.getEntity(String.class));
        response.close();
    }

    private static void avgDevCity(){

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int n = 0;

        while(true){
            try {
                System.out.println("Inserire il numero di misurazioni da leggere:");
                n = Integer.valueOf(br.readLine());
                break;
            } catch (Exception e){
                System.out.println("Inserito carattere non valido, riprova.");
            }
        }

        WebResource target = client.resource("http://localhost:1337/analist").path("get/statistics/average_variance").queryParam("n", String.valueOf(n));
        ClientResponse response = target.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        if(response.getStatus() == 204){
            System.out.println("Non sono ancora state prodotte così tante misure.");
            return;
        }
        System.out.println("RESPONSE FROM CITY: ");
        System.out.println(response.getEntity(String.class));
        response.close();
    }
}
