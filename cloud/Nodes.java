package cloud;

import beans.GlobalLocalStats;
import beans.Node;
import beans.NodesList;
import edges.SimpleNode;
import edges.Stat;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Nodes {

    private List<Node> nodesList;
    private List<Stat> globalStats;
    private List<Stat> localStats;
    private static Nodes instance;

    private Nodes() {
        nodesList = new ArrayList<>();
        globalStats = new ArrayList<>();
        localStats = new ArrayList<>();
    }

    public synchronized static Nodes getInstance(){
        if(instance==null)
            instance = new Nodes();
        return instance;
    }

    public synchronized List<Node> getNodesListCopy() {
        return new ArrayList<>(nodesList);
    }

    public synchronized List<Stat> getGlobalStatsCopy(){
        return new ArrayList<>(globalStats);
    }

    public synchronized Pair<List<Stat>, List<Stat>> getStatsCopy(){
        return new Pair<>(new ArrayList<>(globalStats), new ArrayList<>(localStats));
    }

    public synchronized NodesList addNode(Node n){
        NodesList result = new NodesList();
        for(Node node : nodesList){
            if(node.getId() == n.getId()){
                result.getNodeList().add(new SimpleNode(-1, "-1", -1));
                return result;
            }
            int distance = Math.abs(node.getPosx() - n.getPosx()) + Math.abs(node.getPosy() - n.getPosy());
            if(distance < 20){
                result.getNodeList().add(new SimpleNode(-2, "-2", -2));
                return result;
            }
            if(node.getNodesPort() == n.getNodesPort() || node.getSensorsPort() == n.getSensorsPort() || node.getNodesPort() == n.getSensorsPort() || node.getSensorsPort() == n.getNodesPort()){
                result.getNodeList().add(new SimpleNode(-3, "-3", -3));
                return result;
            }
        }
        /*try{
            Thread.sleep(7000);
        } catch (Exception e){

        }*/

        nodesList.add(n);

        for(Node node : nodesList){
            SimpleNode simple = new SimpleNode(node.getId(), node.getIp(), node.getNodesPort());
            result.getNodeList().add(simple);
        }

        return result;
    }

    public synchronized boolean removeNode(int n){

        for(Node node : nodesList){
            if(node.getId() == n){
                /*try{
                    Thread.sleep(7000);
                } catch (Exception e){

                }*/
                nodesList.remove(node);
                return true;
            }
        }
        return false;
    }

    public synchronized void updateStats(GlobalLocalStats stats){
        globalStats.add(stats.getGlobalStat());
        /*try{
            Thread.sleep(7000);
        } catch (Exception e){

        }*/
        for(Node n : nodesList){
            for(int i = 0; i < stats.getIds().size(); i++){
                if(n.getId() == stats.getIds().get(i)){
                    n.getMeasurements().add(stats.getLocalStats().get(i));
                }
            }
        }
        localStats.addAll(stats.getLocalStats());
    }

    public synchronized Node closestNode(int x, int y){
        int distance = 1000;
        Node result = null;

        /*try{
            Thread.sleep(7000);
        } catch (Exception e){

        }*/


        for(Node n : nodesList){
            int tmp = Math.abs(n.getPosx() - x) + Math.abs(n.getPosy() - y);
            if(tmp < distance){
                distance = tmp;
                result = n;
            }
        }
        return result;
    }

}
