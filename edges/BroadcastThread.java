package edges;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class BroadcastThread extends Thread {
    private SimpleNode simpleNode;
    private String message;
    private Socket clientSocket;
    private DataOutputStream outToServer;
    private BufferedReader inFromServer;
    private Gson gson = new Gson();
    private boolean connectionError = false;

    public BroadcastThread(SimpleNode sn, String message, int id){

        this.message = message;
        simpleNode = sn;

        try
        {
            clientSocket = new Socket(sn.getIp(), sn.getPort());

            outToServer =
                    new DataOutputStream(clientSocket.getOutputStream());

            inFromServer =
                    new BufferedReader(
                            new InputStreamReader(clientSocket.getInputStream()));
        }
        catch (IOException e) {
            connectionError = true;
        }
    }

    public void run(){

        String type = gson.fromJson(message, Message.class).getType();

        try{

            if(type.equals("updateNodesList")){
                outToServer.writeBytes(message);

                String response = inFromServer.readLine();

                //Thread.sleep(5000);

                if(response == null){
                    connectionError = true;
                } else{
                    EdgeNode.setFirstCoordinator(response);
                }

                clientSocket.close();
            }

            if(type.equals("election")){
                outToServer.writeBytes(message);

                String response = inFromServer.readLine();

                if(response.equals("OK")){
                    if(EdgeNode.ok()){
                        //System.out.println("Conferma");
                    }
                } else {
                    if(!response.equals("KO")){
                        connectionError = true;
                    }
                }

                clientSocket.close();
            }

            if(type.equals("head")){
                outToServer.writeBytes(message);

                String response = inFromServer.readLine();

                if(!response.equals("OK")){
                    connectionError = true;
                }

                clientSocket.close();
            }
        } catch (Exception e){
            connectionError = true;
        }

    }

    public SimpleNode getErrorNode(){
        if(connectionError){
            return simpleNode;
        }
        return new SimpleNode(-1,"0",0);
    }
}
