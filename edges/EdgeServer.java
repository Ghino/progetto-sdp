package edges;

import java.io.*;
import java.net.*;

public class EdgeServer extends Thread {

    private ServerSocket socket = null;
    private volatile EdgeThread thread = null;
    private volatile boolean exit = false;

    public EdgeServer(int port) {
        try{
            socket = new ServerSocket(port);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void run() {

        while(!exit){
            try
            {
                Socket connectionSocket = socket.accept();
                thread = new EdgeThread(connectionSocket);
                thread.start();

            }
            catch (IOException e) {
                //System.out.println("eccezione tirata fuori per chiusura improvvisa porte");
            }
        }
    }

    public void close(){
        this.exit = true;
        try{
            if(thread != null){
                if(thread.isAlive()){
                    thread.close();
                    thread.join();
                }
            }
            socket.close();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}