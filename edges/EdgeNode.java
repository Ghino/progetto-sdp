package edges;

import beans.Node;
import beans.NodesList;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import javafx.util.Pair;

import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class EdgeNode {

    private static final Node edge = new Node();
    private static EdgeServer nodeCommunication;
    private static EdgeServer sensorCommunication;
    private static CoordinatorThread coordinatorThread;
    private static SimpleNode coordinator;
    private static Gson gson;
    private static Client client;
    private static final List<SimpleNode> nodeList = new ArrayList<>();
    private static boolean isReady = false;
    private static boolean isElecting = false;
    private static boolean isCSetted = false;
    private static boolean ok = false;
    private static final Object addingLock = new Object();
    private static final Object electionLock = new Object();
    private static final Object coordinatorLock = new Object();
    private static final Object okLock = new Object();

    private EdgeNode(){
    }

    public static void main(String [] args) {

        Random rand = new Random(System.currentTimeMillis());
        gson = new Gson();

        edge.setId(rand.nextInt(100));
        edge.setIp("localhost");
        edge.setSensorsPort(rand.nextInt(2000));
        edge.setNodesPort(rand.nextInt(2000));

        coordinator = new SimpleNode(-1,"0",0);
        coordinatorThread = null;



        //aggiunta nodo edge
        client = Client.create();
        WebResource target = client.resource("http://localhost:1337/node").path("add/node");
        ClientResponse response = null;
        int count = 0;

        while(count < 10){
            edge.setPosx(rand.nextInt(100));
            edge.setPosy(rand.nextInt(100));
            try{
                response = target.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, edge);
            } catch (Exception e){
                System.out.println("Il server non è attivo.");
                return;
            }
            if(response.getStatus() == 200) {
                System.out.println("Nodo entrato!");
                break;
            } else if(response.getStatus() == 406){
                System.out.println("Il nodo ha un ID già esistente nella rete, non può entrare.");
                return;
            } else if(response.getStatus() == 409){
                System.out.println("Il nodo ha un numero di porta dei sensori o dei nodi uguale ad uno già esistente.");
                edge.setNodesPort(rand.nextInt(2000));
                edge.setSensorsPort(rand.nextInt(2000));
            } else{
                count++;
            }
        }
        if(count == 10){
            System.out.println("Il nodo ha fallito ad entrare dopo 10 tentativi.");
            return;
        }

        NodesList result = response.getEntity(NodesList.class);

        response.close();

        nodeList.addAll(result.getNodeList());

        System.out.println(edge.getId());

        nodeCommunication = new EdgeServer(edge.getNodesPort());
        nodeCommunication.start();

        if(nodeList.size() == 1) {
            System.out.println("Primo nodo e quindi coordinatore.");
            coordinator = new SimpleNode(edge.getId(), edge.getIp(), edge.getNodesPort());
            coordinatorThread = new CoordinatorThread(edge); //parte il thread che calcola ogni 5 secondi le medie globali
            coordinatorThread.start();
        } else {
            /*try{
                Thread.sleep(3000);
            } catch (Exception e){
                e.printStackTrace();
            }*/
            //prima cosa da fare è aggiornare i nodi rimanenti
            String message = gson.toJson(new Message("updateNodesList", edge.getId(),
                    new SimpleNode(edge.getId(), edge.getIp(), edge.getNodesPort()))) + "\n";
            List<BroadcastThread> threadList = new ArrayList<>();
            Iterator<SimpleNode> iter = nodeList.iterator();
            int idThread = 0;
            while (iter.hasNext()) {
                SimpleNode sn = iter.next();
                idThread++;
                if (sn.getId() != edge.getId()) {

                    BroadcastThread bt = new BroadcastThread(sn, message, idThread);

                    bt.start();

                    threadList.add(bt);
                }
            }

            for (BroadcastThread bt : threadList) { //attesa di conferma di ogni altro nodo e controllo per rimuovere possibili nodi che non comunicano più
                try {
                    bt.join();
                    if(bt.getErrorNode().getId() != -1){
                        removeNodeFromList(bt.getErrorNode());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(coordinator.getId() == -1){
                coordinator = new SimpleNode(edge.getId(), edge.getIp(), edge.getNodesPort());
                coordinatorThread = new CoordinatorThread(edge);
                coordinatorThread.start();
            }
            System.out.println("Il coordinatore è: " + coordinator.getId());
        }

        synchronized (addingLock){ //confermo di essere pronto a ricevere nuovi nodi
            isReady = true;
            addingLock.notifyAll();
        }

        /*try{
            Thread.sleep(3000);
        } catch (Exception e){
            e.printStackTrace();
        }*/

        //sensori
        sensorCommunication = new EdgeServer(edge.getSensorsPort());
        sensorCommunication.start();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Write \"0\" to close the node.\n");
        try{
            while(true){
                String s = br.readLine();
                if(s.equals("0")){
                    close();
                    break;
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static synchronized void setFirstCoordinator(String message){
        /*try{
            Thread.sleep(3000);
        } catch (Exception e){
            e.printStackTrace();
        }*/
        coordinator = gson.fromJson(message, SimpleNode.class);
    }

    public static void removeNodeFromList(SimpleNode sn){
        synchronized (nodeList){ //gestione thread concorrenti
            /*try{
                Thread.sleep(3000);
            } catch (Exception e){
                e.printStackTrace();
            }*/
            Iterator<SimpleNode> iter = nodeList.iterator();
            while(iter.hasNext()){
                SimpleNode node = iter.next();
                if(sn.getId() == node.getId()){
                    iter.remove();
                }
            }
        }
    }

    public static void updateNodesList(SimpleNode sn, DataOutputStream out){
        synchronized (addingLock) { //attendo finchè questo nodo non ha finito la sua inzializzazione
            while(!isReady){
                try{
                    addingLock.wait();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        synchronized (electionLock){ //se sono in mezzo ad una elezione aspetto che finisca prima di poter comunicare con nuovi possibili nodi
            while(isElecting){
                try{
                    electionLock.wait();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            /*try{
                Thread.sleep(3000);
            } catch (Exception e){
                e.printStackTrace();
            }*/

            synchronized (nodeList){
                nodeList.add(sn);
            }

            synchronized (coordinatorLock){
                try{
                    String updateCoordinator = gson.toJson(coordinator);
                    out.writeBytes(updateCoordinator + "\n");
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    public static void election(DataOutputStream out){ //una elezione non può partire finchè è in corso l'aggiornamento dei nodi

        synchronized (addingLock){//casomai un nuovo nodo venga aggiunto da un altro durante una elezione perchè il nodo che lo aggiunto
            //non si era ancora reso conto di avere una elezione in corso, questo potrebbe ricevere un messaggio di elezione, finchè non è sicuro
            //di essere stato aggiunto da tutti ignora possibili messaggi di elezione
            if(!isReady){
                try{
                    out.writeBytes("KO\n");
                } catch (Exception e){
                    //e.printStackTrace();
                }
                return;
            }
        }

        synchronized (electionLock) {
            try{
                if(out != null){ //se mi arriva elezione da un'altro nodo confermo di esserci e di averla avviata
                    out.writeBytes("OK" + "\n");
                }
            } catch (Exception e) {
                //e.printStackTrace();
            }

            /*try{
                Thread.sleep(3000);
            } catch (Exception e){
                e.printStackTrace();
            }*/

            if (isElecting) { //se sono già in elezione non deve ripartire
                return;
            }
            synchronized (coordinatorLock){
                if(isCSetted){ //se mi parte una elezione per via di un messaggio intermedio che ha superato la prima lock in sendMeasurement
                    // ma nel frattempo era arrivato un nuovo coordinatore
                    electionLock.notifyAll();
                    return;
                }
            }
            isElecting = true;
        }

        System.out.println("Inizio elezione");
        //fa elezione con tutti i nodi più grandi di lui
        List<BroadcastThread> threadList = new ArrayList<>();
        String message = gson.toJson(new Message("election", edge.getId(), new SimpleNode(edge.getId(), edge.getIp(), edge.getNodesPort()))) + "\n";

        synchronized (nodeList){
            Iterator<SimpleNode> iter = nodeList.iterator();

            while (iter.hasNext()) {
                SimpleNode sn = iter.next();
                if(sn.getId() != edge.getId()){
                    if (sn.getId() > edge.getId()) {
                        BroadcastThread bt = new BroadcastThread(sn, message, 0);

                        bt.start();

                        threadList.add(bt);
                    }
                }
            }
        }

        for (BroadcastThread bt : threadList) { //aspetto che tutti i nodi abbiano ricevuto messaggio di elezione e elimino eventuali scomparsi
            try {
                bt.join();
                if(bt.getErrorNode().getId() != -1){
                    removeNodeFromList(bt.getErrorNode());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*try{
            Thread.sleep(3000);
        } catch (Exception e){
            e.printStackTrace();
        }*/

        if (!ok) { //se il nodo non riceve nemmeno un ok diventa coordinatore e avverte tutti
            System.out.println("Sono coordinatore: " + edge.getId());

            /*try{
                Thread.sleep(3000);
            } catch (Exception e){
                e.printStackTrace();
            }*/

            synchronized (coordinatorLock){
                coordinator = new SimpleNode(edge.getId(), edge.getIp(), edge.getNodesPort());
                coordinatorThread = new CoordinatorThread(edge);
                coordinatorThread.start();
                isCSetted = true;
            }

            threadList = new ArrayList<>();
            message = gson.toJson(new Message("head", edge.getId(), new SimpleNode(edge.getId(), edge.getIp(), edge.getNodesPort()))) + "\n";

            synchronized (nodeList){
                Iterator<SimpleNode> iter1 = nodeList.iterator();

                while (iter1.hasNext()) {
                    SimpleNode sn = iter1.next();
                    if (sn.getId() != edge.getId()) {
                        BroadcastThread bt = new BroadcastThread(sn, message, 0);

                        bt.start();

                        threadList.add(bt);
                    }
                }
            }

            for (BroadcastThread bt : threadList) { //aspetto che tutti i nodi abbiano confermato la ricezione del coordinatore e rimuovo eventuali scomparsi
                try {
                    bt.join();
                    if(bt.getErrorNode().getId() != -1){
                        removeNodeFromList(bt.getErrorNode());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else { //se riceve ok aspetta che arrivi il coordinatore e solo se il coordinatore non è stato già scelto

            /*try{
                Thread.sleep(3000);
            } catch (Exception e){
                e.printStackTrace();
            }*/

            synchronized (coordinatorLock) {
                System.out.println("Non sono coordinatore");
                while(!isCSetted){ //se non mi era arrivato ancora un coordinatore lo aspetto, altrimenti finisco tranquillamente la elezione
                    try {
                        coordinatorLock.wait(10000);
                        if(!isCSetted){
                            //se il candidato coordinatore muore prima di riuscire a darlo agli altri nodi, si deve far ripartire elezione
                            synchronized (electionLock){
                                ok = false;
                                isElecting = false;
                            }
                            System.out.println("Il candidato coordinatore è morto, si ricomincia elezione");
                            election(null);
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        synchronized (electionLock){ //resetto i paramentri che mi facevano andare in wait e risveglio tutti
            ok = false;
            System.out.println("Elezione finita");
            isElecting = false;
            electionLock.notifyAll();
        }
    }

    public static boolean ok(){
        /*try{
            Thread.sleep(3000);
        } catch (Exception e){
            e.printStackTrace();
        }*/
        synchronized (okLock){
            ok = true;
        }
        return true;
    }

    public static void setCoordinator(SimpleNode coordinatorNode, DataOutputStream out){
        synchronized (coordinatorLock){
            /*try{
                Thread.sleep(3000);
            } catch (Exception e){
                e.printStackTrace();
            }*/
            coordinator = coordinatorNode;
            try{
                out.writeBytes("OK" + "\n");
            } catch(Exception e){
                e.printStackTrace();
            }
            isCSetted = true;
            System.out.println("Coordinatore settato: " + coordinatorNode.getId());
            coordinatorLock.notify(); //una volta che si ottiene il coordinatore nuovo sblocco il nodo che rimaneva in attesa
        }
    }

    public static void sendAverage(Stat s){

        synchronized (electionLock) {
            while (isElecting) { //se è in elezione aspetta il nuovo coordinatore
                try {
                    electionLock.wait();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        boolean error = false;

        /*try{
            Thread.sleep(3000);
        } catch (Exception e){
            e.printStackTrace();
        }*/

        synchronized (coordinatorLock){
            if (coordinator.getId() == edge.getId()) {
                //se è lo stesso nodo evito di mandare un messaggio a me stesso e aggiorno direttamente le misurazioni globali
                updateGlobalStats(new Pair<>(edge.getId(), s));//in caso di server veri usare ip per il controllo
                return;
            }

            try {

                Socket clientSocket = new Socket(coordinator.getIp(), coordinator.getPort());

                DataOutputStream outToServer =
                        new DataOutputStream(clientSocket.getOutputStream());

                outToServer.writeBytes(gson.toJson(new Message("average", edge.getId(), s)) + "\n");

                BufferedReader inFromServer =
                        new BufferedReader(
                                new InputStreamReader(clientSocket.getInputStream()));

                Message inMessage = gson.fromJson(inFromServer.readLine(), Message.class);

                clientSocket.close();

                /*try{
                    Thread.sleep(3000);
                } catch (Exception e){
                    e.printStackTrace();
                }*/

                if (inMessage.getType().equals("globalAverage")) {
                    if (inMessage.getStat().getValue() != 0 && inMessage.getStat().getTimestamp() != 0) {
                        System.out.println("MEDIA GLOBALE DAL COORDINATORE: " + inMessage.getStat().getValue() + " " + inMessage.getStat().getTimestamp());
                    } else {
                        System.out.println("Il coordinatore ha 0 medie globali calcolate.");
                    }
                }

            } catch (Exception e) { //se il coordinatore muore si perde solo la statistica che ha fatto partire l`eccezione
                //resetto il check del coordinatore in seguito ad una possibile elezione se il nuovo coordinatore muore
                isCSetted = false;
                error = true;
            }
        }

        if(error){
            election(null);
        }

    }

    public static Stat updateGlobalStats(Pair<Integer, Stat> stat){
        return coordinatorThread.updateGetLastStat(stat);
    }

    private static void close(){
        System.out.println("Chiusura in corso, rimozione nodo edge.");
        //rimozione nodo edge
        WebResource target = client.resource("http://localhost:1337/node").path("close/nodes/" + edge.getId());
        ClientResponse response = target.delete(ClientResponse.class);
        response.close();
        client.destroy();

        //aspetto che si chiudano tutti i thread in modo corretto prima di chiudere tutto

        nodeCommunication.close();
        sensorCommunication.close();
        if(coordinatorThread != null){
            if(coordinatorThread.isAlive()){
                coordinatorThread.close();
            }
        }
        try{
            nodeCommunication.join();
            sensorCommunication.join();
            if(coordinatorThread != null){
                if(coordinatorThread.isAlive()){
                    coordinatorThread.join();
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        /*synchronized (electionLock){
            electionLock.notifyAll();
        }
        synchronized (coordinatorLock){
            coordinatorLock.notify();
        }*/
        System.out.println("CHIUSE TUTTE LE CONNESSIONI!");
        System.exit(0);
    }
}
