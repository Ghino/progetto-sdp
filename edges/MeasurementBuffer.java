package edges;

import sensors.Measurement;

import java.util.ArrayList;
import java.util.List;

public class MeasurementBuffer {

    private static MeasurementBuffer instance = null;
    private List<Measurement> buffer;

    private MeasurementBuffer(){

        buffer = new ArrayList<>();
    }

    public synchronized static MeasurementBuffer getInstance(){
        if(instance == null){
            instance = new MeasurementBuffer();
        }

        return instance;
    }

    public synchronized Stat addMeasure(Measurement m) {
        buffer.add(m);
        if(buffer.size() == 40){
            double average = 0;
            for(int i = 0; i < buffer.size(); i++){
                average += buffer.get(i).getValue();
            }
            average /= buffer.size();
            Stat stat = new Stat(average, System.currentTimeMillis());

            //trasmissione al coordinatore

            /*try{
                Thread.sleep(2000);
            }catch(Exception e){
                e.printStackTrace();
            }*/

            System.out.println("Statistica locale: " + stat.getValue() + " " + stat.getTimestamp());

            for(int i = 0; i < 20; i++){
                buffer.remove(i);
            }

            return stat;
        }

        return new Stat(0,0);
    }
}