package edges;

import com.google.gson.Gson;
import javafx.util.Pair;

import java.io.*;
import java.net.Socket;

public class EdgeThread extends Thread {

    private Socket socket;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;
    private Gson gson = new Gson();
    private volatile boolean exit = false;

    public EdgeThread(Socket s){

        socket = s;

        try
        {
            inFromClient =
                    new BufferedReader(
                            new InputStreamReader(socket.getInputStream()));

            outToClient =
                    new DataOutputStream(socket.getOutputStream());

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run(){

        Message inMessage;

        try {

            inMessage = gson.fromJson(inFromClient.readLine(), Message.class);

            if(inMessage.getType().equals("measurement")){
                while(socket.isConnected() && !exit){
                    Stat average = MeasurementBuffer.getInstance().addMeasure(inMessage.getMeasurement());
                    if(average.getValue() != 0 && average.getTimestamp() != 0){
                        EdgeNode.sendAverage(average);
                    }
                    outToClient.writeBytes("OK" + "\n");
                    inMessage = gson.fromJson(inFromClient.readLine(), Message.class);
                }
            }
            if(inMessage.getType().equals("updateNodesList")){
                //Thread.sleep(10000);
                EdgeNode.updateNodesList(inMessage.getNode(), outToClient);
            }
            if(inMessage.getType().equals("election")){
                //Thread.sleep(2000);
                EdgeNode.election(outToClient);
            }
            if(inMessage.getType().equals("head")){
                //Thread.sleep(2000);
                EdgeNode.setCoordinator(inMessage.getNode(), outToClient);
            }
            if(inMessage.getType().equals("average")){
                //Thread.sleep(2000);
                Stat lastGlobal = EdgeNode.updateGlobalStats(new Pair<>(inMessage.getId(), inMessage.getStat()));
                outToClient.writeBytes(gson.toJson(new Message("globalAverage", -1, lastGlobal)) + "\n");
            }
            socket.close();

        }
        catch (Exception e) {
            //System.out.println("Errore di connessione.");
        }
    }

    public void close(){
        this.exit = true;
        try{
            socket.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
