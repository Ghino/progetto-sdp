package edges;

import beans.GlobalLocalStats;
import beans.Node;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import javafx.util.Pair;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

public class CoordinatorThread extends Thread {

    private List<Stat> globalList = new ArrayList<>();
    private List<Pair<Integer, Stat>> localList = new ArrayList<>();
    private Node node;
    private Client client;
    private volatile boolean exit = false;

    public CoordinatorThread(Node n){
        node = n;
        client = Client.create();
    }

    public void run(){
        while(!exit){

            try{
                Thread.sleep(5000);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //calcolo statistiche
            synchronized (localList){
                if(!localList.isEmpty()){
                    double average = 0;
                    List<Integer> ids = new ArrayList<>();
                    List<Stat> locals = new ArrayList<>();
                    System.out.println("STATISTICHE LOCALI GIUNTE AL COORDINATORE: ");
                    for(Pair<Integer, Stat> p : localList){
                        System.out.println(p.getValue().getValue() + " " + p.getValue().getTimestamp());
                        ids.add(p.getKey());
                        locals.add(p.getValue());
                        average += p.getValue().getValue();
                    }
                    average /= localList.size();

                    long timestamp = System.currentTimeMillis();

                    System.out.println("Calcolata media globale: " + average + " " + timestamp);

                    Stat global = new Stat(average, timestamp);
                    globalList.add(global);

                    localList.clear();

                /*try{
                    Thread.sleep(3000);
                } catch (Exception e){
                    e.printStackTrace();
                }*/

                    GlobalLocalStats values = new GlobalLocalStats();
                    values.setGlobalStat(global);
                    values.setIds(ids);
                    values.setLocalStats(locals);

                    WebResource target = client.resource("http://localhost:1337/node").path("update/stats");
                    ClientResponse response = target.type(MediaType.APPLICATION_JSON).put(ClientResponse.class, values);
                    response.close();
                }
            }
        }
        System.out.println("CHIUSO COORDINATORE!");
        client.destroy();
    }

    public Stat updateGetLastStat(Pair<Integer, Stat> stat){

        synchronized (localList){
            this.localList.add(stat);

            if(globalList.isEmpty()){
                return new Stat(0,0);
            }
            return globalList.get(globalList.size() - 1);
        }
    }

    public void close(){
        this.exit = true;
    }
}
