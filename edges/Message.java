package edges;

import sensors.Measurement;

public class Message {

    private String type;
    private int id;
    private SimpleNode node;
    private Stat stat;
    private Measurement m;

    public Message(String type, int id, SimpleNode node){
        this.type = type;
        this.id = id;
        this.node = node;
    }

    public Message(String type, int id, Stat stat){
        this.type = type;
        this.id = id;
        this.stat = stat;
    }

    public Message(String type, Measurement m){
        this.type = type;
        this.m = m;
    }

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public SimpleNode getNode(){
        return node;
    }

    public Stat getStat() {
        return stat;
    }

    public Measurement getMeasurement(){
        return m;
    }
}
