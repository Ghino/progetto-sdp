package sensors;

public interface SensorStream {

    void sendMeasurement(Measurement m);

}
