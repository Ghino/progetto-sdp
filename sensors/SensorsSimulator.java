package sensors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SensorsSimulator {

    private static String server = "http://localhost:1337";

    private static List<Sensor> senList = new ArrayList<>();
    private static List<Simulator> simList = new ArrayList<>();

    public static void main(String[] args){

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while(true){
            System.out.println("Inserisci numero di sensori:");

            try {
                int value = Integer.valueOf(br.readLine());

                for(int i = 0; i < value; i++){
                    Sensor sensor = new Sensor();
                    senList.add(sensor);
                    Simulator sim = new PM10Simulator(sensor);
                    sim.start();
                }

                System.out.println("Tutti i sensori sono partiti.");
                break;
            } catch(IOException e) {
                System.out.println("Carattere non valido, riprova.");
            }
        }


        while(true){
            try {

                Thread.sleep(10000);
                System.out.println("Ricerca nodi...");
                for(Sensor s : senList){
                    s.checkConnection();
                }

            } catch(Exception e) {
                e.printStackTrace();
            }
        }

    }
}
