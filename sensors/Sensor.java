package sensors;

import beans.Node;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import edges.Message;

import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Random;

public class Sensor implements SensorStream{

    private Node nodo;
    private int posX;
    private int posY;
    private Client client = Client.create();
    private Gson gson = new Gson();
    private Socket clientSocket;
    private DataOutputStream outToServer;

    public Sensor(){
        Random rand = new Random();
        posX = rand.nextInt(100);
        posY = rand.nextInt(100);

        WebResource target = client.resource("http://localhost:1337/sensor").path("find").queryParam("x", String.valueOf(posX)).queryParam("y", String.valueOf(posY));
        ClientResponse response = target.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if(response.getStatus() == 204){
            System.out.println("Nessun nodo edge esistente.");
        } else{
            nodo = response.getEntity(Node.class);
            System.out.println("Sensore connesso al " + nodo.getId() + " nodo.");
        }
        response.close();

        try{
            clientSocket = new Socket(nodo.getIp(), nodo.getSensorsPort());

            outToServer =
                    new DataOutputStream(clientSocket.getOutputStream());
        } catch (Exception e){
            //e.printStackTrace();
        }
    }

    @Override
    public void sendMeasurement(Measurement m) {

        try{

            synchronized (outToServer){
                outToServer.writeBytes(gson.toJson(new Message("measurement", m)) + "\n");
            }

        } catch(Exception e) {
            //e.printStackTrace();
            System.out.println("Connessione mancante, ricerca nuovo nodo.");
            checkConnection();
        }

    }

    public void checkConnection(){

        Node tmp = nodo;

        WebResource target = client.resource("http://localhost:1337/sensor").path("find").queryParam("x", String.valueOf(posX)).queryParam("y", String.valueOf(posY));
        ClientResponse response = target.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if(response.getStatus() == 204){
            System.out.println("Nessun nodo edge esistente.");
        } else{
            nodo = response.getEntity(Node.class);
            System.out.println("Sensore connesso al " + nodo.getId() + " nodo.");
        }
        response.close();

        if(nodo.getId() != tmp.getId()){
            try{
                synchronized (outToServer){
                    clientSocket = new Socket(nodo.getIp(), nodo.getSensorsPort());

                    outToServer =
                            new DataOutputStream(clientSocket.getOutputStream());
                }
            } catch (Exception e){
                //e.printStackTrace();
            }
        }
    }
}
