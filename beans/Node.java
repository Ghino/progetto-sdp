package beans;

import edges.Stat;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Node {

    private int id;
    private String ip;
    private int sensorsPort;
    private int nodesPort;
    private int posx;
    private int posy;

    private List<Stat> measurements;

    public Node(){
        measurements = new ArrayList<>();
    }

    public int getId(){
        return id;
    }

    public String getIp() {
        return ip;
    }

    public int getSensorsPort() {
        return sensorsPort;
    }

    public int getNodesPort() {
        return nodesPort;
    }

    public int getPosx() {
        return posx;
    }

    public int getPosy() {
        return posy;
    }

    public List<Stat> getMeasurements() {
        return measurements;
    }

    public List<Stat> getNMeasurements(int n) {

        List<Stat> values = measurements;
        List<Stat> result = new ArrayList<>();

        if(n > values.size()){
            return null;
        }
        for(int i = values.size() - n; i < values.size(); i++){
            result.add(values.get(i));
        }
        return result;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setSensorsPort(int sensorsPort) {
        this.sensorsPort = sensorsPort;
    }

    public void setNodesPort(int nodesPort) {
        this.nodesPort = nodesPort;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public void setMeasurements(List<Stat> measurements) {
        this.measurements = measurements;
    }
}
