package beans;

import edges.Stat;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Measurements {

    private List<Stat> valuesList;

    public Measurements() {
        this.valuesList = new ArrayList<>();
    }

    public List<Stat> getValuesList() {
        return valuesList;
    }

    public void setValuesList(List<Stat> valuesList) {
        this.valuesList = valuesList;
    }
}
