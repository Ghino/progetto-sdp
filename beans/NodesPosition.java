package beans;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class NodesPosition {

    @XmlElement(name="positions")
    private List<NodePosition> positions;

    public NodesPosition(){
        positions = new ArrayList<>();
    }

    public List<NodePosition> getPositions() {
        return positions;
    }

    public void setPositions(List<NodePosition> positions) {
        this.positions = positions;
    }
}
