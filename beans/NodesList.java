package beans;

import edges.SimpleNode;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class NodesList {

    private List<SimpleNode> nodeList = new ArrayList<>();

    public NodesList() {
    }

    public NodesList(List<SimpleNode> nodeList) {
        this.nodeList = nodeList;
    }

    public List<SimpleNode> getNodeList() {
        return nodeList;
    }

    public void setNodeList(List<SimpleNode> nodeList) {
        this.nodeList = nodeList;
    }
}
