package beans;

import edges.Stat;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GlobalLocalStats {

    private Stat globalStat;
    private List<Stat> localStats = new ArrayList<>();
    private List<Integer> ids = new ArrayList<>();

    public GlobalLocalStats(){

    }

    public Stat getGlobalStat() {
        return globalStat;
    }

    public void setGlobalStat(Stat globalStat) {
        this.globalStat = globalStat;
    }

    public List<Stat> getLocalStats() {
        return localStats;
    }

    public void setLocalStats(List<Stat> localStats) {
        this.localStats = localStats;
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void setIds(List<Integer> ids) {
        this.ids = ids;
    }
}
