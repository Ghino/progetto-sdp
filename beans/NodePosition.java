package beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NodePosition {

    private int id;
    private int x;
    private int y;

    public NodePosition(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
